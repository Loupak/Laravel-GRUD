<?php

use Illuminate\Database\Seeder;
use App\Item;

class ItemsTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();

        foreach (range(1, 100) as $index) 
        {
        	Item::create([
        		'title' => $faker->sentence($nbWords = 4),
        		'description' => $faker->paragraph($nbSentences = 7)	
        	]);
        }
    }
}
